/*

  A Magic Wallpaper
  		Copyright (C) 2019  Jorge Eliécer Sanabria Hernández

  		This program is free software: you can redistribute it and/or modify
  		it under the terms of the GNU General Public License as published by
  		the Free Software Foundation, either version 3 of the License, or
  		(at your option) any later version.

  		This program is distributed in the hope that it will be useful,
  		but WITHOUT ANY WARRANTY; without even the implied warranty of
  		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  		GNU General Public License for more details.

  		You should have received a copy of the GNU General Public License
  		along with this program.  If not, see <http://www.gnu.org/licenses/>.

  		jesanabriah@gmail.com

 */

package com.alicesmagic.android.magicpaper;

import java.util.Random;

import android.graphics.Bitmap;
import android.os.SystemClock;

@SuppressWarnings("DuplicateBranchesInSwitch")
class Mosca {
	
	private static final long duracion_aleteo = 100;
	private static final long duracion_matarmosca = 5000;
	
	private long mStartTimeAnimation;
	private long tAnimation = 0;
	
	public static final int MOSCA_INICIO = 0;
	public static final int MOSCA_ENTRO = 1;
	public static final int MOSCA_SALIO = 2;
	public static final int MOSCA_MUERTA = 3;
	
	// --Commented out by Inspection (21/01/19 07:12 PM):private boolean isOut = true;
	private int estado = MOSCA_INICIO;
	
	
	//Posicion inicial
	private float xPos0 = 0;
	private float yPos0 = 0;
	
	//Posicion inicial
	private float xPos_Muerta = 0;
	private float yPos_Muerta = 0;
	
	//Direccion
	private float xDir;
	private float yDir;
	
	public static Bitmap mosca0;
	public static Bitmap mosca1;
	public static Bitmap mosca2;
	
	public static Bitmap b;
	
	private boolean isAbierta = true;
	private boolean isMatarMosca = false;
	
	public Mosca() {							
		//Valor por defecto para la direccion
		setAnguloRandom();
		mStartTimeAnimation = SystemClock.elapsedRealtime();
	}
	
	public Boolean isPointOnMosca(float x, float y, float t, float radio) {
		
		float dx = getxPos(t) - x;
		float dy = getyPos(t) - y;
		float distancia = (float) Math.hypot(dx, dy);

		return distancia < radio;

	}
	
	public void matarMosca(float t){
		setxyPos_Muerta(getxPos(t), getyPos(t));
		setMatarMosca(true);
	}
	
	private void setMatarMosca(boolean value){
		isMatarMosca = value;	
		
		if(value){
			if(estado != MOSCA_MUERTA){
				mStartTimeAnimation = SystemClock.elapsedRealtime();
				tAnimation = 0;
				estado = MOSCA_MUERTA;
			}
		}
		else{
			if(estado == MOSCA_MUERTA){
				//isOut = true;
				mStartTimeAnimation = SystemClock.elapsedRealtime();
				tAnimation = 0;
				estado = MOSCA_INICIO;
			}
		}
	}
	
	public boolean isMatarMosca(){
		return isMatarMosca;
	}
	
	public boolean aleteo(){

		tAnimation = SystemClock.elapsedRealtime() - mStartTimeAnimation;
		if(isMatarMosca){
			if(tAnimation > duracion_matarmosca){
				setMatarMosca(false);
			}
		}
		else{
			if(tAnimation > duracion_aleteo){
				mStartTimeAnimation = SystemClock.elapsedRealtime();
				tAnimation = 0;
				
				isAbierta = !isAbierta;
				return !isAbierta;
			}
		}
		return isAbierta;
	}

	public void setPos0(float xPos0, float yPos0) {
		setxPos0(xPos0);
		setyPos0(yPos0);
	}
	
// --Commented out by Inspection START (21/01/19 07:04 PM):
//	/**
//	 * @return the xPos0
//	 */
//	public float getxPos0() {
//		return xPos0;
//	}
// --Commented out by Inspection STOP (21/01/19 07:04 PM)

	/**
	 * @param xPos0 the xPos0 to set
	 */
	private void setxPos0(float xPos0) {
		this.xPos0 = xPos0;
	}

// --Commented out by Inspection START (21/01/19 07:05 PM):
//	/**
//	 * @return the yPos0
//	 */
//	public float getyPos0() {
//		return yPos0;
//	}
// --Commented out by Inspection STOP (21/01/19 07:05 PM)

	/**
	 * @param yPos0 the yPos to set
	 */
	private void setyPos0(float yPos0) {
		this.yPos0 = yPos0;
	}
	
	/**
	 * @return the xPos
	 */
	public float getxPos(float t) {
		return (getxDir()*t) + xPos0;
	}

	/**
	 * @return the yPos
	 */
	public float getyPos(float t) {
		return (getyDir()*t) + yPos0;
	}

	/**
	 * @return the xDir
	 */
	public float getxDir() {
		return xDir;
	}

	/**
	 * @return the yDir
	 */
	public float getyDir() {
		return yDir;
	}

	/**
	 * @param angulo the angulo to set
	 */
	private void setAngulo(float angulo) {
		this.xDir = (float) (Math.cos(angulo)/4);
		this.yDir = (float) (Math.sin(angulo)/4);
	}

	public void setAnguloRandom() {
		float angulo;
		Random r = new Random();
		angulo = r.nextFloat()*2*(float)Math.PI;
		setAngulo(angulo);
		this.xDir = (float) (Math.cos(angulo)/4);
		this.yDir = (float) (Math.sin(angulo)/4);
	}

	public float getAngulo() {
		return (float)(Math.atan2(yDir, xDir)/Math.PI*180 + 90);
	}

// --Commented out by Inspection START (21/01/19 07:10 PM):
//	public void setxyDir(float xDir, float yDir) {
//		this.xDir = xDir;
//		this.yDir = yDir;
//	}
// --Commented out by Inspection STOP (21/01/19 07:10 PM)

// --Commented out by Inspection START (21/01/19 07:06 PM):
//	/**
//	 * @return the isOut
//	 */
//	public boolean isOut() {
//		return isOut;
//	}
// --Commented out by Inspection STOP (21/01/19 07:06 PM)

	/**
	 * @param isOut the isOut to set
	 */
	public void setOut(boolean isOut) {
		//this.isOut = isOut;
		
		switch(estado){
		case MOSCA_INICIO:
			if(!isOut){
				estado = MOSCA_ENTRO;
			}
			break;
		case MOSCA_ENTRO:
			if(isOut){
				estado = MOSCA_SALIO;
			}
			break;
		case MOSCA_SALIO:
			if(!isOut){
				estado = MOSCA_ENTRO;
			}
			break;
		case MOSCA_MUERTA:
			//estado = MOSCA_MUERTA;
			break;	
		}
	}

	/**
	 * @return the estado
	 */
	public int getEstado() {
		return estado;
	}

	/**
	 * @return the xPos_Muerta
	 */
	public float getxPos_Muerta() {
		return xPos_Muerta;
	}

// --Commented out by Inspection START (21/01/19 07:09 PM):
//	/**
//	 * @param xPos_Muerta the xPos_Muerta to set
//	 */
//	public void setxPos_Muerta(float xPos_Muerta) {
//		this.xPos_Muerta = xPos_Muerta;
//	}
// --Commented out by Inspection STOP (21/01/19 07:09 PM)

	/**
	 * @return the yPos_Muerta
	 */
	public float getyPos_Muerta() {
		return yPos_Muerta;
	}

// --Commented out by Inspection START (21/01/19 07:10 PM):
//	/**
//	 * @param yPos_Muerta the yPos_Muerta to set
//	 */
//	public void setyPos_Muerta(float yPos_Muerta) {
//		this.yPos_Muerta = yPos_Muerta;
//	}
// --Commented out by Inspection STOP (21/01/19 07:10 PM)

	private void setxyPos_Muerta(float xPos_Muerta, float yPos_Muerta) {
		this.xPos_Muerta = xPos_Muerta;
		this.yPos_Muerta = yPos_Muerta;
	}
}
