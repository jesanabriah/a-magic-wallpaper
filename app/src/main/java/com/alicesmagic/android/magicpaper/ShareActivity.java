/*

  A Magic Wallpaper
  		Copyright (C) 2019  Jorge Eliécer Sanabria Hernández

  		This program is free software: you can redistribute it and/or modify
  		it under the terms of the GNU General Public License as published by
  		the Free Software Foundation, either version 3 of the License, or
  		(at your option) any later version.

  		This program is distributed in the hope that it will be useful,
  		but WITHOUT ANY WARRANTY; without even the implied warranty of
  		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  		GNU General Public License for more details.

  		You should have received a copy of the GNU General Public License
  		along with this program.  If not, see <http://www.gnu.org/licenses/>.

  		jesanabriah@gmail.com

 */

package com.alicesmagic.android.magicpaper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

public class ShareActivity extends Activity {
    
	private static final int HORIZONTAL_MEDIA_IMAGE_REQUEST_CODE = 0;
	private static final int VERTICAL_MEDIA_IMAGE_REQUEST_CODE = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		boolean use_horizontal_image = getSharedPreferences(MagicPaper.SHARED_PREFS_NAME, 0)
				.getBoolean("use_horizontal_image", false);
		
		if(use_horizontal_image){
			showAlertDialog();
		}
		else{
			useImage();
		}	    
	}

	@SuppressWarnings("ConstantConditions")
	private void useImage() {
   	    // Get the intent that started this activity
   	    Intent intent = getIntent();
   	    String action = intent.getAction();
   	    String type = intent.getType();
   	    if (Intent.ACTION_SEND.equals(action) && type != null) {
   	        if (type.startsWith("image/")) {
   	            handleSendImage(intent, VERTICAL_MEDIA_IMAGE_REQUEST_CODE); // Handle image being sent
   	        }
   	    }	
        
   	    //Start new intent
		Intent i = new Intent();

		if(Build.VERSION.SDK_INT > 15){
		    i.setAction(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);

		    String p = MagicPaper.class.getPackage().getName();
		    String c = MagicPaper.class.getCanonicalName();        
		    i.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(p, c));
		}
		else{
		    i.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
		}
		
		startActivity(i);
		
        setResult(RESULT_OK);
        finish();
	}

	private void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    
	    builder.setIcon(R.drawable.ic_launcher);
	    builder.setTitle(R.string.share_dialog_title);
	    builder.setItems(R.array.dialog_items, new DialogInterface.OnClickListener() {
	        @SuppressWarnings("ConstantConditions")
			@SuppressLint("InlinedApi")
			public void onClick(DialogInterface dialog, int which) {
	       	    // Get the intent that started this activity
	       	    Intent intent = getIntent();
	       	    String action = intent.getAction();
	       	    String type = intent.getType();
	
	       	    if (Intent.ACTION_SEND.equals(action) && type != null) {
	       	        if (type.startsWith("image/")) {
	       	            handleSendImage(intent, which); // Handle image being sent
	       	        }
	       	    }	
	            
	       	    //Start new intent
	    		Intent i = new Intent();

	    		if(Build.VERSION.SDK_INT > 15){
	    		    i.setAction(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);

	    		    String p = MagicPaper.class.getPackage().getName();
	    		    String c = MagicPaper.class.getCanonicalName();        
	    		    i.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(p, c));
	    		}
	    		else{
	    		    i.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
	    		}
	    		
	    		startActivity(i);
	    		
	            setResult(RESULT_OK);
	            finish();
	        }
	    });
	    
	    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
                setResult(RESULT_CANCELED);
                finish();
			}
		});
	    
	    AlertDialog dialog = builder.create();	           	

	    dialog.show();		
	}

	private void handleSendImage(Intent intent, int requestCode) {
	    Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
	    if (imageUri != null) {
			String[] filePathColumn = {MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
			
			String picturePath = "0";

			if(cursor != null){
				if(cursor.moveToFirst()){
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					picturePath = cursor.getString(columnIndex);
					cursor.close();
				}
			}
			else{
				picturePath = imageUri.getPath();
			}
			
            Editor editor = getSharedPreferences(MagicPaper.SHARED_PREFS_NAME, 0).edit();
            
            switch(requestCode) {
            case HORIZONTAL_MEDIA_IMAGE_REQUEST_CODE:
            	editor.putString("user_horizontal_picture", picturePath);
                break;
            case VERTICAL_MEDIA_IMAGE_REQUEST_CODE:
            	editor.putString("user_vertical_picture", picturePath);
                break;
            }
            
            editor.apply();
	    }		
	}
}
