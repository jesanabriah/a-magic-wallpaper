/*

  A Magic Wallpaper
  		Copyright (C) 2019  Jorge Eliécer Sanabria Hernández

  		This program is free software: you can redistribute it and/or modify
  		it under the terms of the GNU General Public License as published by
  		the Free Software Foundation, either version 3 of the License, or
  		(at your option) any later version.

  		This program is distributed in the hope that it will be useful,
  		but WITHOUT ANY WARRANTY; without even the implied warranty of
  		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  		GNU General Public License for more details.

  		You should have received a copy of the GNU General Public License
  		along with this program.  If not, see <http://www.gnu.org/licenses/>.

  		jesanabriah@gmail.com

 */

package com.alicesmagic.android.magicpaper;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.provider.MediaStore;
import androidx.core.content.ContextCompat;
import android.widget.Toast;

@SuppressWarnings("ALL")
public class MagicPaperSettings extends PreferenceActivity
    implements SharedPreferences.OnSharedPreferenceChangeListener {
	
	private static final int HORIZONTAL_MEDIA_IMAGE_REQUEST_CODE = 0;
	private static final int VERTICAL_MEDIA_IMAGE_REQUEST_CODE = 1;
    
	@Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);        

        getPreferenceManager().setSharedPreferencesName(MagicPaper.SHARED_PREFS_NAME);
        addPreferencesFromResource(R.xml.magicpaper_settings);
        
        Preference customPref_vertical = findPreference("user_vertical_picture");
        customPref_vertical.setOnPreferenceClickListener(
                new OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                    	Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                        startActivityForResult(i, VERTICAL_MEDIA_IMAGE_REQUEST_CODE);                        
                        return true;
                    }
                });
        
        Preference customPref_horizontal = findPreference("user_horizontal_picture");
        customPref_horizontal.setOnPreferenceClickListener(
                new OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {                    	
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                        startActivityForResult(i, HORIZONTAL_MEDIA_IMAGE_REQUEST_CODE);
                    	return true;
                    }
                });
        
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    
    }

	@Override
    protected void onDestroy() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

	
    public void onSharedPreferenceChanged(SharedPreferences mPrefs, String key) {
    	
    }

	@SuppressWarnings("ConstantConditions")
    protected final void onActivityResult(final int requestCode, final int resultCode, final Intent i) {
        super.onActivityResult(requestCode, resultCode, i);

        //REvisa los permisos de lectura
        if (ContextCompat.checkSelfPermission(MagicPaperSettings.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED){

            if(resultCode == RESULT_OK &&
                    (requestCode == HORIZONTAL_MEDIA_IMAGE_REQUEST_CODE ||
                    requestCode == VERTICAL_MEDIA_IMAGE_REQUEST_CODE)) {

                Uri cImageUri = i.getData();

                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(cImageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                SharedPreferences sp = getPreferenceManager().getSharedPreferences();
                Editor editor = sp.edit();

                switch(requestCode) {
                case HORIZONTAL_MEDIA_IMAGE_REQUEST_CODE:
                    editor.putString("user_horizontal_picture", picturePath);
                    break;
                case VERTICAL_MEDIA_IMAGE_REQUEST_CODE:
                    editor.putString("user_vertical_picture", picturePath);
                    break;
                }

                //editor.commit();
                editor.apply();
            }
        }
        else{
                Toast.makeText(MagicPaperSettings.this, getString(R.string.without_permits), Toast.LENGTH_LONG).show();
        }
    }
}
