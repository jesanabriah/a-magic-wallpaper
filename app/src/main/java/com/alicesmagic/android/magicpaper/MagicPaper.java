/*

  A Magic Wallpaper
  		Copyright (C) 2019  Jorge Eliécer Sanabria Hernández

  		This program is free software: you can redistribute it and/or modify
  		it under the terms of the GNU General Public License as published by
  		the Free Software Foundation, either version 3 of the License, or
  		(at your option) any later version.

  		This program is distributed in the hope that it will be useful,
  		but WITHOUT ANY WARRANTY; without even the implied warranty of
  		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  		GNU General Public License for more details.

  		You should have received a copy of the GNU General Public License
  		along with this program.  If not, see <http://www.gnu.org/licenses/>.

  		jesanabriah@gmail.com

 */

package com.alicesmagic.android.magicpaper;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.SystemClock;
import android.service.wallpaper.WallpaperService;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.Random;

@SuppressWarnings("ALL")
public class MagicPaper extends WallpaperService {

    public static final String SHARED_PREFS_NAME="magicpapersettings";

	@Override
    public Engine onCreateEngine() {
        return new TableroEngine();
    }

    class TableroEngine extends Engine 
    implements SharedPreferences.OnSharedPreferenceChangeListener{
    	
        private final Handler mHandler = new Handler(); 
        
        private long tam_imagen = 0;
        
        private float rapidez = 2.5f;
        
		private final Paint mPaint = new Paint();
		private Bitmap background;       
        
		private long mStartTime = 0;
        private long t = 0; //Tiempo ultima actualizacion
        private long tpas = 0; //Tiempo pasado ultima actualizacion
        private float t_rapidez = 0;
        
        private Object[] objetos;

        private float xTam = 0;
        private float yTam = 0;
        
        private float mOffset = 0;
        private float mTouchX = -1;
        private float mTouchY = -1;
        
        private int img_width = 0;
        private int img_height = 0;        
        private int xoffset = 0;
        private int yoffset = 0;

		private boolean isImgHorizontal = true;
        private boolean is_custom_image_loaded = false;
        private boolean use_custom_image = false;

		//Preferences - Animation constants

    	private static final int T_RANDOM = 0;
    	private static final int T_NULL = 1;
    	private static final int T_CIRCLES = 2;
    	private static final int T_NIEVE = 3;
    	private static final int T_MOSCAS = 4; 
    	private static final int T_GLOBOS = 5; 
    	private static final int T_CORAZONES = 6;
    	private static final int T_RAYOSX = 7;
    	private static final int T_CLOCK = 8;
    	private static final int NUM_ANIMATIONS = 7;
    	
    	//Preferences - Animation constants
    	private static final int B_RELLENAR = 0;
    	private static final int B_AJUSTAR = 1;
    	private static final int B_EXPANDIR = 2;
    	private static final int B_MOSAICO = 3;
    	private static final int B_CENTRO = 4;
    	
    	//Preferences - General
        private boolean is_scroll = false;
        private int animation = T_NULL;
        private int vertical_animation = T_NULL;
        private int horizontal_animation = T_NULL;
		private float angulo_horizontal = 0;
        private float angulo_vertical = 0;
        
        //Preferences - Wallpapers
    	private int horizontal_ajuste = B_RELLENAR;
    	private int vertical_ajuste = B_RELLENAR;
    	private String user_vertical_picture = "0";
    	private String user_horizontal_picture = "0";
    	
    	//Preferences - Circles
        private float circles_radio = 10;
        private int circles_densidad = 10;        
    	
    	//Preferences - RayosX
        private float rayosx_radio = 100;
        
    	//Preference - Nieve
    	private int nieve_densidad = 50;
    	private float nieve_radio = 4;
    	
    	//Preference - Mosca
    	private float moscas_radio = 50;
    	private int moscas_cantidad = 1;
    	private boolean moscas_pelean = true;
    	
    	//Preference - Globos
    	private int globos_densidad = 50;
    	private float globos_radio = 25;
    	
    	//Preference - Corazones
    	private int corazones_densidad = 50;
    	private float corazones_radio = 25;
    	
    	//Preference - Clock
    	//private int corazones_densidad = 50;
    	//private float corazones_radio = 25;
    	
    	private final int[] FONDOS = {
    			R.drawable.b_null,
    			R.drawable.b_circles, 
    			R.drawable.b_nieve, 
    			R.drawable.b_moscas, 
    			R.drawable.b_globos, 
    			R.drawable.b_corazones,
    			R.drawable.b_rayosx,
    			R.drawable.b_clock
    			};
    	
        private final Runnable mDrawTablet = new Runnable() {
            @Override
			public void run() {drawTablet();}
        };

        TableroEngine() {        	
            setTouchEventsEnabled(true);
        }

		public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
			//Preference - General
			vertical_animation = Integer.valueOf(prefs.getString("vertical_animation", "0"));
			//RATED = prefs.getInt("RATED", 1);
			rapidez = ((float)prefs.getInt("speed", 50))/20f;//Factor de escala
			is_scroll = prefs.getBoolean("is_scroll", false);

			//REvisa los permisos de lectura
            if (ContextCompat.checkSelfPermission(MagicPaper.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                use_custom_image = prefs.getBoolean("use_custom_image", false);
            }
            else{
                use_custom_image = false;
            }

			angulo_horizontal = Integer.valueOf(prefs.getString("horizontal_rotation", "0"));
			angulo_vertical = Integer.valueOf(prefs.getString("vertical_rotation", "0"));
			
			//Preference - Wallpapers
			//Vertical
			user_vertical_picture = prefs.getString("user_vertical_picture", "0");
			vertical_ajuste = Integer.valueOf(prefs.getString("vertical_ajuste", "0"));
			
			//Horizontal
			boolean use_vertical_image_on_horizontal = !prefs.getBoolean("use_horizontal_image", false);
			if(use_vertical_image_on_horizontal){user_horizontal_picture = user_vertical_picture;}
			else{user_horizontal_picture = prefs.getString("user_horizontal_picture", "0");}

			boolean use_horizontal_animation = prefs.getBoolean("use_horizontal_animation", false);
			if(!use_horizontal_animation){horizontal_animation = vertical_animation;}
			else{horizontal_animation = Integer.valueOf(prefs.getString("horizontal_animation", "0"));}
			
			horizontal_ajuste = Integer.valueOf(prefs.getString("horizontal_ajuste", "0"));
			
			//Preference - Animations			
			//Preference - Circles
			circles_radio = (float) prefs.getInt("circles_radio", 10);
			circles_densidad = prefs.getInt("circles_densidad", 10);			

			//Preference - Nieve
			nieve_radio = (float) prefs.getInt("nieve_radio", 4);
			nieve_densidad = prefs.getInt("nieve_densidad", 50);
			
			//Preference - Moscas
			moscas_radio = ((float) prefs.getInt("moscas_tam", 75)) / 2;
			moscas_cantidad = prefs.getInt("moscas_cantidad", 1);
			moscas_pelean = prefs.getBoolean("moscas_pelean", true);
			
			//Preference - Globos
			globos_radio = (float) prefs.getInt("globos_radio", 25);
			globos_densidad = prefs.getInt("globos_densidad", 50);
			
			//Preference - Corazones
			corazones_radio = (float) prefs.getInt("corazones_radio", 25);
			corazones_densidad = prefs.getInt("corazones_densidad", 50);
			
			//Preference - RayosX
			rayosx_radio = (float) prefs.getInt("rayosx_radio", 100);
			      
			//Preference - Clock
			//rayosx_radio = (float) prefs.getInt("rayosx_radio", 100);
			
            //Valores iniciales de la animacion
            createAnimation();     
            
            //Ajustar en memoria la imagen de fondo
            //setBgImageAnimation();             
		}
		
		private void createAnimation(){
            
			if(yTam < xTam){//Horizontal
				if(horizontal_animation == T_RANDOM){
					Random r = new Random();
					animation = r.nextInt(NUM_ANIMATIONS) + 2;
				}
				else{
					animation = horizontal_animation;
				}
			}
			else if(yTam > xTam){//Vertical
				if(vertical_animation == T_RANDOM){
					Random r = new Random();
					animation = r.nextInt(NUM_ANIMATIONS) + 2;
				}
				else{
					animation = vertical_animation;
				}
			}	
			else{
				animation = T_NULL;
			}
			
			switch(animation){
            case T_NULL:
            	onCreateNull();
            	break;
            case T_CIRCLES:
            	onCreateCircles();
            	break;
            case T_NIEVE:
            	onCreateNieve();
            	break;
            case T_MOSCAS:
            	onCreateMoscas();
            	break;
            case T_GLOBOS:
            	onCreateGlobos();
            	break;
            case T_CORAZONES:
            	onCreateCorazones();
            	break;
            case T_RAYOSX:
            	onCreateRayosX();
            	break;
            case T_CLOCK:
            	onCreateClock();
            	break;
            }
            
			//Resetear valores iniciales generales
            resetValues();
            
            setBgImage();            
		}
        
        @Override
        public void onTouchEvent (MotionEvent event) {
            super.onTouchEvent(event);            
            
            switch(animation){
            case T_NULL:
            	break;
            case T_CIRCLES:
            	onCirclesTouchEvent(event);
            	break;
            case T_NIEVE:
            	break;
            case T_MOSCAS:
            	onMoscasTouchEvent(event);
            	break;
            case T_GLOBOS:
            	//onGlobosTouchEvent(event);//Para explotar los globos
            	break;
            case T_CORAZONES:
            	//onCorazonesTouchEvent(event);//Para explotar los globos
            	break;
            case T_RAYOSX:
            	onRayosXTouchEvent(event);//Para explotar los globos
            	break;
            case T_CLOCK:
            	//onClockTouchEvent(event);//Para explotar los globos
            	break;
            }
        }

        private void onCirclesTouchEvent(MotionEvent event){
            
        	if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mTouchX = event.getX();
                mTouchY = event.getY();
				for (Object objeto : objetos) {
					if (((Circulo) objeto).isPointOnCircle(mTouchX, mTouchY, t_rapidez, circles_radio)) {
						((Circulo) objeto).setRandomColor();
						return;
					}
				}
				for (Object objeto : objetos) {
					((Circulo) objeto).setRandomColor();
				}
            }
        	
        }
        
        private void onMoscasTouchEvent(MotionEvent event){
            mTouchX = event.getX();
            mTouchY = event.getY();

			for (Object objeto : objetos) {
				if (((Mosca) objeto).isPointOnMosca(mTouchX + moscas_radio, mTouchY + moscas_radio, t_rapidez, moscas_radio) && !((Mosca) objeto).isMatarMosca()) {
					((Mosca) objeto).matarMosca(t_rapidez);
				}
			}
        }

		private void onRayosXTouchEvent(MotionEvent event){
        	
        	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	        	((RayosX) objetos[0]).setPos0(event.getX(), event.getY());
	        	((RayosX) objetos[0]).setManual(true);
        	}
        	else if(event.getAction() == MotionEvent.ACTION_UP){
        		resetValues();
        		((RayosX) objetos[0]).setManual(false);
        	}
        	else{
        		resetValues();
        		((RayosX) objetos[0]).setPos0(event.getX(), event.getY());
        	}
        }

		@Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder); 
            
            //Calcular tamano de la pantalla
            DisplayMetrics outMetrics = new DisplayMetrics();          
            ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getMetrics(outMetrics);           
            xTam = outMetrics.widthPixels;
            yTam = outMetrics.heightPixels; 
            
            //Shared preference changed
			SharedPreferences mPrefs = MagicPaper.this.getSharedPreferences(SHARED_PREFS_NAME, 0);
            mPrefs.registerOnSharedPreferenceChangeListener(this);
            onSharedPreferenceChanged(mPrefs, null);
        }
               
        private void onCreateNieve() {
        	//Paint
			mPaint.reset();
            mPaint.setAntiAlias(true);                                                         
        
        	int nieve_cantidad = (int) (xTam*yTam/(10*nieve_radio*nieve_radio*4)*nieve_densidad/50);
			
        	if(nieve_cantidad >1000){nieve_cantidad = 1000;}
        	else if(nieve_cantidad < 50){nieve_cantidad = 50;}
        	
        	objetos = new Nieve[nieve_cantidad];
        	
            for (int i = 0; i < objetos.length; i++){objetos[i] = new Nieve();}        	        	
        	
            Drawable d_copo_nieve = getResources().getDrawable(R.drawable.snow);
            
            Nieve.copo_nieve = Bitmap.createBitmap(d_copo_nieve.getIntrinsicWidth(),
					d_copo_nieve.getIntrinsicHeight(),
            		Bitmap.Config.ARGB_8888);
            
            d_copo_nieve.setBounds(0, 0,
					d_copo_nieve.getIntrinsicWidth(),
					d_copo_nieve.getIntrinsicHeight());

            d_copo_nieve.draw(new Canvas(Nieve.copo_nieve));
            
            randomNievePositions();
		}
        
        private void onCreateGlobos() {                                                       
        	//Paint
			mPaint.reset();
            mPaint.setAntiAlias(true); 
            
        	int globos_cantidad = (int) (xTam*yTam/(10*globos_radio*globos_radio*4)*globos_densidad/20);
			
        	if(globos_cantidad >50){globos_cantidad = 50;}
        	else if(globos_cantidad < 5){globos_cantidad = 5;}
        	
        	objetos = new Globo[globos_cantidad];
        	
            for (int i = 0; i < objetos.length; i++){objetos[i] = new Globo();}        	        	
        	
            Drawable d_globo = getResources().getDrawable(R.drawable.globo_amarillo);            
            Globo.globo_amarillo = Bitmap.createBitmap((int) globos_radio*2 + 4, (int) (globos_radio*2 + 4)*3, Bitmap.Config.ARGB_8888);            
            d_globo.setBounds(0, 0, (int) (globos_radio*2 + 4), (int) (globos_radio*2 + 4)*3);
            d_globo.draw(new Canvas(Globo.globo_amarillo));
            
            d_globo = getResources().getDrawable(R.drawable.globo_azul);            
            Globo.globo_azul = Bitmap.createBitmap((int) globos_radio*2 + 4, (int) (globos_radio*2 + 4)*3, Bitmap.Config.ARGB_8888);
            d_globo.setBounds(0, 0, (int) (globos_radio*2 + 4), (int) (globos_radio*2 + 4)*3);
            d_globo.draw(new Canvas(Globo.globo_azul));
            
            d_globo = getResources().getDrawable(R.drawable.globo_rojo);
            Globo.globo_rojo = Bitmap.createBitmap((int) globos_radio*2 + 4, (int) (globos_radio*2 + 4)*3, Bitmap.Config.ARGB_8888);            
            d_globo.setBounds(0, 0, (int) (globos_radio*2 + 4), (int) (globos_radio*2 + 4)*3);
            d_globo.draw(new Canvas(Globo.globo_rojo));
            
            d_globo = getResources().getDrawable(R.drawable.globo_verde);            
            Globo.globo_verde = Bitmap.createBitmap((int) globos_radio*2 + 4, (int) (globos_radio*2 + 4)*3, Bitmap.Config.ARGB_8888);            
            d_globo.setBounds(0, 0, (int) (globos_radio*2 + 4), (int) (globos_radio*2 + 4)*3);
            d_globo.draw(new Canvas(Globo.globo_verde));
            
            randomGlobosPositions();
		}
        
        private void onCreateCorazones() {                                                   
        	//Paint
			mPaint.reset();
            mPaint.setAntiAlias(true); 
            
        	int corazones_cantidad = (int) (xTam*yTam/(10*corazones_radio*corazones_radio*4)*corazones_densidad/20);
			
        	if(corazones_cantidad >50){corazones_cantidad = 50;}
        	else if(corazones_cantidad < 5){corazones_cantidad = 5;}
        	
        	objetos = new Globo[corazones_cantidad];
        	
            for (int i = 0; i < objetos.length; i++){objetos[i] = new Globo();}        	        	
        	
            Drawable d_corazon = getResources().getDrawable(R.drawable.corazon);
            
            Globo.globo = Bitmap.createBitmap((int) (corazones_radio*2 + 4), (int) (corazones_radio*2 + 4), Bitmap.Config.ARGB_8888);
            
            d_corazon.setBounds(0, 0, (int) (corazones_radio*2 + 4), (int) (corazones_radio*2 + 4));
            d_corazon.draw(new Canvas(Globo.globo));
            
            randomCorazonesPositions();
		}
        
        private void onCreateMoscas() {
        	//Paint
			mPaint.reset();
            mPaint.setAntiAlias(true); 
        	objetos = new Mosca[moscas_cantidad];
        	
            for (int i = 0; i < objetos.length; i++){
	            objetos[i] = new Mosca();
            }        	        	
        	
            //Mosca muerta
            Drawable d_mosca = getResources().getDrawable(R.drawable.mosca0);            
            Mosca.mosca0 = Bitmap.createBitmap((int) moscas_radio*2, (int) moscas_radio*2, Bitmap.Config.ARGB_8888);
            d_mosca.setBounds(0, 0, (int) moscas_radio*2, (int) moscas_radio*2);
            d_mosca.draw(new Canvas(Mosca.mosca0));
            
            //Mosca 1
            d_mosca = getResources().getDrawable(R.drawable.mosca1);            
            Mosca.mosca1 = Bitmap.createBitmap((int) moscas_radio*2, (int) moscas_radio*2, Bitmap.Config.ARGB_8888);
            d_mosca.setBounds(0, 0, (int) moscas_radio*2, (int) moscas_radio*2);
            d_mosca.draw(new Canvas(Mosca.mosca1));
            
            //Mosca 2
            d_mosca = getResources().getDrawable(R.drawable.mosca2);            
            Mosca.mosca2 = Bitmap.createBitmap((int) moscas_radio*2, (int) moscas_radio*2, Bitmap.Config.ARGB_8888);
            d_mosca.setBounds(0, 0, (int) moscas_radio*2, (int) moscas_radio*2);
            d_mosca.draw(new Canvas(Mosca.mosca2));
            
            //Bitmap temporal
            Mosca.b = Bitmap.createBitmap((int)(moscas_radio*2), (int)(moscas_radio*2), Bitmap.Config.ARGB_8888);
            
            randomMoscasPositions();
		}

		private void onCreateNull() {
			objetos = null;			
		}		

		private void onCreateCircles() {
        	//Paintm
			mPaint.reset();
            mPaint.setAntiAlias(true); 
            mPaint.setStrokeWidth(2);
        	mPaint.setStyle(Style.STROKE);                                                         
 
        	int circles_cantidad = (int) (xTam*yTam/(10*circles_radio*circles_radio*4)*circles_densidad/100);
        			
        	if(circles_cantidad >200){circles_cantidad = 200;}
        	else if(circles_cantidad == 0){circles_cantidad = 1;}
        	
        	objetos = new Circulo[circles_cantidad];
        	
            for (int i = 0; i < objetos.length; i++){objetos[i] = new Circulo();}        	        	
        	
            Random r = new Random();
        	//Numero burbujas
			for (Object objeto : objetos) {
				((Circulo) objeto).setPos0(0, 0);
			}
            for (int i = 0; i < objetos.length; i++){
            	do{
            		((Circulo) objetos[i]).setPos0(r.nextFloat()*(xTam - 2*circles_radio) + circles_radio, 
            										r.nextFloat()*(yTam - 2*circles_radio) + circles_radio);                		
            	}
            	while(isCircleOnCircles(i)); 
            }
		}		
		
		private void onCreateRayosX() {
        	//Paint
			mPaint.reset();
			mPaint.setAntiAlias(false);
            mPaint.setStyle(Style.FILL); 
 
            rayosx_radio = Math.min(Math.min(xTam/3, yTam/3), rayosx_radio);
            
        	objetos = new RayosX[1];        	
            objetos[0] = new RayosX();        	        	
        	
            Random r = new Random();
            
            ((RayosX) objetos[0]).setPos0(r.nextFloat()*(xTam - 2*rayosx_radio) + rayosx_radio, r.nextFloat()*(yTam - 2*rayosx_radio) + rayosx_radio);
            
            Drawable lente = getResources().getDrawable(R.drawable.lente);         
            ((RayosX) objetos[0]).lente = Bitmap.createBitmap((int)(2*rayosx_radio), (int)(2*rayosx_radio), Bitmap.Config.ALPHA_8);
            lente.setBounds(0, 0, (int)(2*rayosx_radio), (int)(2*rayosx_radio));
            lente.draw(new Canvas(((RayosX) objetos[0]).lente));
		}
		
		private void onCreateClock() {
			objetos = null;			
		}

		private void setBgImage(){

			try{
		        //Guardar la imagen de fondo en memoria           
		        Drawable drawable;
		        
	            if(yTam > xTam){//Vertical
	            	isImgHorizontal = false;
	
	            	if(!use_custom_image){
	        			drawable = getResources().getDrawable(FONDOS[animation-1]);
	            		ajusteVertical(getBitmapRotate(drawable, angulo_vertical));        			
	            	}
	            	else if(user_vertical_picture.equals("0")){
	        			is_custom_image_loaded = false;
	        		}
	        		else{
	            	    //REvisa los permisos de lectura
                        if (ContextCompat.checkSelfPermission(MagicPaper.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED) {

                            File prueba = new File(user_vertical_picture);
                            tam_imagen = prueba.length();
                            if (prueba.exists()) {  //1Mb
                                drawable = Drawable.createFromPath(user_vertical_picture);
								//noinspection ConstantConditions
								ajusteVertical(getBitmapRotate(drawable, angulo_vertical));
                                is_custom_image_loaded = true;
                            } else {
                                is_custom_image_loaded = false;
                            }

                        }
                        else{
                            Toast.makeText(MagicPaper.this, getString(R.string.without_permits), Toast.LENGTH_LONG).show();
                        }
	        		}
	            }
	            else{//Horizontal
	            	isImgHorizontal = true;
	        		
	            	if(!use_custom_image){//Si imagen == imagen predefinida
	            		drawable = getResources().getDrawable(FONDOS[animation-1]);
	            		ajusteHorizontal(getBitmapRotate(drawable, angulo_horizontal));
	            	}
	            	else if(user_horizontal_picture.equals("0")){
	        			is_custom_image_loaded = false;
	        		}
	        		else{
                        //REvisa los permisos de lectura
                        if (ContextCompat.checkSelfPermission(MagicPaper.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED) {

                            File prueba = new File(user_horizontal_picture);
                            tam_imagen = prueba.length();
                            if(prueba.exists()){         //Unlimited
                                drawable = Drawable.createFromPath(user_horizontal_picture);
								//noinspection ConstantConditions
								ajusteHorizontal(getBitmapRotate(drawable, angulo_horizontal));
                                is_custom_image_loaded = true;
                            }
                            else{
                                is_custom_image_loaded = false;
                            }

                        }
                        else{
                            Toast.makeText(MagicPaper.this, getString(R.string.without_permits), Toast.LENGTH_LONG).show();
                        }
	        		}        		           	
	            }
			}
	    	catch(java.lang.OutOfMemoryError iae){
	    		//Log.d(this.toString(), "drawAnimation()", iae);   
	    		is_custom_image_loaded = false;
	    	} 
		}
		
		private float getEscala(int w, int h){					
			float hs = ((float)h)/1000f;
			float ws = ((float)w)/1000f;
			float xs = xTam/1000f;
			float ys = yTam/1000f;

			float f_image = ws * hs;
            float f_screen = xs * ys;

			if(f_image > f_screen){
				return 1.5625f;//Este factor necesario por si se habilita el desplazamiento entre escritorios
			}
			else{
				return 1.5625f*f_screen/f_image;//Importante para que imagenes pequeñas se vean bien
			}
		}

		private Bitmap getBitmapRotate(Drawable drawable, float angulo) {
			
			Canvas canvita;
			Bitmap drawableBitmap;
			
			int h = drawable.getIntrinsicHeight();
			int w = drawable.getIntrinsicWidth();
			
			float escala = getEscala(w, h);
			
			if(angulo == 90){
				drawableBitmap = Bitmap.createBitmap((int)(h*escala), (int)(w*escala), Bitmap.Config.ARGB_8888);				
    			canvita = new Canvas(drawableBitmap);        			
    			canvita.save();
    			canvita.rotate(90);
    			canvita.translate(0, -drawableBitmap.getWidth());
    			drawable.setBounds(0, 0, (int)(w*escala), (int)(h*escala));
    			drawable.draw(canvita);
    			canvita.restore();
			}
			else if(angulo == 180){
				drawableBitmap = Bitmap.createBitmap((int)(w*escala), (int)(h*escala), Bitmap.Config.ARGB_8888);
				canvita = new Canvas(drawableBitmap); 
    			canvita.save();
    			canvita.rotate(180);
    			canvita.translate(-drawableBitmap.getWidth(), -drawableBitmap.getHeight());
    			drawable.setBounds(0, 0, (int)(w*escala), (int)(h*escala));
    			drawable.draw(canvita);
    			canvita.restore();
			}
			else if(angulo == 270){
				drawableBitmap = Bitmap.createBitmap((int)(h*escala), (int)(w*escala), Bitmap.Config.ARGB_8888);
				canvita = new Canvas(drawableBitmap); 
    			canvita.save();
    			canvita.rotate(270);
    			canvita.translate(-drawableBitmap.getHeight(), 0);
    			drawable.setBounds(0, 0, (int)(w*escala), (int)(h*escala));
    			drawable.draw(canvita);
    			canvita.restore();
			}
			else{				
				drawableBitmap = Bitmap.createBitmap((int)(w*escala), (int)(h*escala), Bitmap.Config.ARGB_8888);
				canvita = new Canvas(drawableBitmap); 
    			canvita.save();
    			drawable.setBounds(0, 0, (int)(w*escala), (int)(h*escala));
    			drawable.draw(canvita);
    			canvita.restore();
			}
			return drawableBitmap;
		}
				
		private void ajusteVertical(Bitmap drawableBitmap) {

			background = null;
			Rect rect = new Rect();
			switch(vertical_ajuste){
        	case B_RELLENAR:
            	img_width = (int) (drawableBitmap.getWidth()*yTam/drawableBitmap.getHeight());
            	img_height = (int) yTam;
            	
            	if(img_width < xTam){
            		img_width = (int) xTam;
            		img_height = (int) (drawableBitmap.getHeight()*xTam/drawableBitmap.getWidth());
            	}
                xoffset = (int)(xTam - img_width)/2;
                yoffset = (int)(yTam - img_height)/2;
                
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(xTam * 1.25f), (int)(yTam * 1.25f), Bitmap.Config.ARGB_8888);
                	rect = new Rect((int)(xoffset * 1.25f), (int)(yoffset * 1.25f), (int)(img_width * 1.25f) + (int)(xoffset * 1.25f), (int)(img_height * 1.25f) + (int)(yoffset * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap((int)xTam, (int)yTam, Bitmap.Config.ARGB_8888);
                	rect = new Rect(xoffset, yoffset, img_width + xoffset, img_height + yoffset);
                } 
        		break;
        	case B_AJUSTAR:
            	img_width = (int) (drawableBitmap.getWidth()*yTam/drawableBitmap.getHeight());
            	img_height = (int) yTam;
            	
            	if(img_width > xTam){
            		img_width = (int) xTam;
            		img_height = (int) (drawableBitmap.getHeight()*xTam/drawableBitmap.getWidth());
            	}
                xoffset = (int)(xTam - img_width)/2;
                yoffset = (int)(yTam - img_height)/2;
                
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(xTam * 1.25f), (int)(yTam * 1.25f), Bitmap.Config.ARGB_8888);
                	rect = new Rect((int)(xoffset * 1.25f), (int)(yoffset * 1.25f), (int)(img_width * 1.25f) + (int)(xoffset * 1.25f), (int)(img_height * 1.25f) + (int)(yoffset * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap((int)xTam, (int)yTam, Bitmap.Config.ARGB_8888);
                	rect = new Rect(xoffset, yoffset, img_width + xoffset, img_height + yoffset);
                } 
        		break;
        	case B_EXPANDIR:
            	img_width = (int) xTam;
            	img_height = (int) yTam;
            	
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(xTam * 1.25f), (int)(yTam * 1.25f), Bitmap.Config.ARGB_8888);
                	rect = new Rect(0, 0, (int)(img_width * 1.25f), (int)(img_height * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap((int)xTam, (int)yTam, Bitmap.Config.ARGB_8888);
                    rect = new Rect(0, 0, img_width, img_height);
                } 
        		break;
        	case B_MOSAICO:
            	img_width = drawableBitmap.getWidth();
            	img_height = drawableBitmap.getHeight();
            	
            	if(img_width > xTam){
            		img_width = (int) xTam;
            		img_height = (int) (drawableBitmap.getHeight() * xTam / drawableBitmap.getWidth());
            	}
            	if(img_height > yTam){
            		img_width = (int) (img_width * yTam / img_height);
            		img_height = (int) yTam;
            	}
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(img_width * 1.25f), (int)(img_height * 1.25f), Bitmap.Config.ARGB_8888);
                    rect = new Rect(0, 0, (int)(img_width * 1.25f), (int)(img_height * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap(img_width, img_height, Bitmap.Config.ARGB_8888);
                    rect = new Rect(0, 0, img_width, img_height);
                } 
        		break;
        	case B_CENTRO:
            	img_width = drawableBitmap.getWidth();
            	img_height = drawableBitmap.getHeight();
                xoffset = (int)(xTam - img_width)/2;
                yoffset = (int)(yTam - img_height)/2;
                
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(xTam * 1.25f), (int)(yTam * 1.25f), Bitmap.Config.ARGB_8888);
                    rect = new Rect((int)(xoffset * 1.25f), (int)(yoffset * 1.25f), (int)(img_width * 1.25f) + (int)(xoffset * 1.25f), (int)(img_height * 1.25f) + (int)(yoffset * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap((int)xTam, (int)yTam, Bitmap.Config.ARGB_8888);
                    rect = new Rect(xoffset, yoffset, img_width + xoffset, img_height + yoffset);
                } 
        		break;
        	}
			
            Canvas canvita = new Canvas(background);
            canvita.drawBitmap(drawableBitmap, null, rect, mPaint);
		}

		private void ajusteHorizontal(Bitmap drawableBitmap) {
    		
			background = null;
			Rect rect = new Rect();
			switch(horizontal_ajuste){
        	case B_RELLENAR:
            	img_width = (int) (xTam);
            	img_height = (int) (drawableBitmap.getHeight()*xTam/drawableBitmap.getWidth());
            	
            	if(img_height < yTam){
            		img_height = (int) (yTam);
            		img_width= (int) (drawableBitmap.getWidth()*yTam/drawableBitmap.getHeight());
            	}
                xoffset = (int)(xTam - img_width)/2;
                yoffset = (int)(yTam - img_height)/2;
            	
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(xTam * 1.25f), (int)(yTam * 1.25f), Bitmap.Config.ARGB_8888);
                    rect = new Rect((int)(xoffset * 1.25f), (int)(yoffset * 1.25f), (int)(img_width * 1.25f) + (int)(xoffset * 1.25f), (int)(img_height * 1.25f) + (int)(yoffset * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap((int)(xTam), (int)(yTam), Bitmap.Config.ARGB_8888);
                    rect = new Rect(xoffset, yoffset, img_width + xoffset, img_height + yoffset);
                } 
        		break;
        	case B_AJUSTAR:
            	img_width = (int) (xTam);
            	img_height = (int) (drawableBitmap.getHeight()*xTam/drawableBitmap.getWidth());            	
            	
            	if(img_height > yTam){
            		img_height = (int) (yTam);
            		img_width= (int) (drawableBitmap.getWidth()*yTam/drawableBitmap.getHeight());
            	}
                xoffset = (int)(xTam - img_width)/2;
                yoffset = (int)(yTam - img_height)/2;                
            	
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(xTam * 1.25f), (int)(yTam * 1.25f), Bitmap.Config.ARGB_8888);
                    rect = new Rect((int)(xoffset * 1.25f), (int)(yoffset * 1.25f), (int)(img_width * 1.25f) + (int)(xoffset * 1.25f), (int)(img_height * 1.25f) + (int)(yoffset * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap((int)(xTam), (int)(yTam), Bitmap.Config.ARGB_8888);
                    rect = new Rect(xoffset, yoffset, img_width + xoffset, img_height + yoffset);
                } 
        		break;
        	case B_EXPANDIR:
            	img_width = (int) (xTam);
            	img_height = (int) (yTam);            	
            	
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(xTam * 1.25f), (int)(yTam * 1.25f), Bitmap.Config.ARGB_8888);
                    rect = new Rect(0, 0, (int)(img_width * 1.25f), (int)(img_height * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap((int)(xTam), (int)(yTam), Bitmap.Config.ARGB_8888);                    
            		rect = new Rect(0, 0, img_width, img_height);
                } 
        		break;
        	case B_MOSAICO:
            	img_width = drawableBitmap.getWidth();
            	img_height = drawableBitmap.getHeight();
            	
            	if(img_width > xTam){
            		img_width = (int) (xTam);
            		img_height = (int) (drawableBitmap.getHeight() * xTam / drawableBitmap.getWidth());
            	}
            	if(img_height > yTam){
            		img_width = (int) (img_width * yTam / img_height);
            		img_height = (int) (yTam);
            	}            	
            	
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(img_width * 1.25f), (int)(img_height * 1.25f), Bitmap.Config.ARGB_8888);
                    rect = new Rect(0, 0, (int)(img_width * 1.25f), (int)(img_height * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap(img_width, img_height, Bitmap.Config.ARGB_8888);
                    rect = new Rect(0, 0, img_width, img_height);
                } 
        		break;
        	case B_CENTRO:
            	img_width = drawableBitmap.getWidth();
            	img_height = drawableBitmap.getHeight();
                xoffset = (int)(xTam - img_width)/2;
                yoffset = (int)(yTam - img_height)/2;
                
                if(is_scroll){
                	background = Bitmap.createBitmap((int)(xTam * 1.25f), (int)(yTam * 1.25f), Bitmap.Config.ARGB_8888);
                    rect = new Rect((int)(xoffset * 1.25f), (int)(yoffset * 1.25f), (int)(img_width * 1.25f) + (int)(xoffset * 1.25f), (int)(img_height * 1.25f) + (int)(yoffset * 1.25f));
                }
                else{
                	background = Bitmap.createBitmap((int)(xTam), (int)(yTam), Bitmap.Config.ARGB_8888);
                    rect = new Rect(xoffset, yoffset, img_width + xoffset, img_height + yoffset);
                } 
        		break;
        	} 

            Canvas canvita = new Canvas(background);
            canvita.drawBitmap(drawableBitmap, null, rect, mPaint);
		}

		@Override
        public void onDestroy() {
            super.onDestroy();
            
            mHandler.removeCallbacks(mDrawTablet);

        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);                   	                        
                 
            if(isVisible() || isPreview()){
	            switch(animation){
	            case T_NULL:
	            	//nada por hacer...
	            	break;
	            case T_CIRCLES:
	            	randomCirclesPositions();
	            	break;
	            case T_NIEVE:
	            	randomNievePositions();
	            	break;
	            case T_MOSCAS:
	            	randomMoscasPositions();
	            	break;
	            case T_GLOBOS:
	            	randomGlobosPositions();
	            	break;
	            case T_CORAZONES:
	            	randomCorazonesPositions();
	            	break;
	            case T_RAYOSX:
	            	randomRayosXPositions();
	            	break;
	            case T_CLOCK:
	            	//randomClockPositions();
	            	break;
	            }           
	            resetValues();
	            mHandler.postDelayed(mDrawTablet, 50);//delay black
            }
            
        }
        
        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
                  
            xTam = width;
            yTam = height;   
            
            if(isVisible() || isPreview()){
	            createAnimation();
	            mHandler.postDelayed(mDrawTablet, 50);//delay black
            }            
        }
        
        private void resetValues() {
            mTouchX = -1;
            mTouchY = -1; 
            
        	mStartTime = SystemClock.elapsedRealtime();    
        	tpas = 0;
            t = 0;	
            t_rapidez = 0;
		}

		private void randomRayosXPositions() {
            Random r = new Random();
        	//Numero burbujas
            ((RayosX) objetos[0]).setPos0(r.nextFloat()*(xTam - 2*rayosx_radio) + rayosx_radio, 
            								r.nextFloat()*(yTam - 2*rayosx_radio) + rayosx_radio);              
		}
		
		private void randomCirclesPositions() {
            Random r = new Random();

			for (Object objeto : objetos) {
				((Circulo) objeto).setPos0(0, 0);
			}
            for (int i = 0; i < objetos.length; i++){
            	do{
            		((Circulo) objetos[i]).setPos0(r.nextFloat()*(xTam - 2*circles_radio) + circles_radio, 
            										r.nextFloat()*(yTam - 2*circles_radio) + circles_radio);                		
            	}
            	while(isCircleOnCircles(i)); 
            }			
		}
		
		private void randomNievePositions() {
            Random r = new Random();
        	//Numero burbujas
			for (Object objeto : objetos) {//((Nieve) objetos[i]).getRadio()*2 + 4
				((Nieve) objeto).setRadioRandom(nieve_radio);
				((Nieve) objeto).setPos0(r.nextFloat() * (xTam + ((Nieve) objeto).getRadio() * 2 + 4),
						r.nextFloat() * (yTam + ((Nieve) objeto).getRadio() * 2 + 4));
			}
		}
		
		private void randomGlobosPositions() {
            Random r = new Random();
        	//Numero burbujas
			for (Object objeto : objetos) {//((Nieve) objetos[i]).getRadio()*2 + 4
				((Globo) objeto).setRadioRandom(globos_radio);
				((Globo) objeto).setPos0(r.nextFloat() * (xTam), r.nextFloat() * (yTam));
			}
		}
		
		private void randomCorazonesPositions() {
            Random r = new Random();
        	//Numero burbujas
			for (Object objeto : objetos) {//((Nieve) objetos[i]).getRadio()*2 + 4
				((Globo) objeto).setRadioRandom(corazones_radio);
				((Globo) objeto).setPos0(r.nextFloat() * (xTam), r.nextFloat() * (yTam));
			}
		}
		
		private void randomMoscasPositions() {
            Random r = new Random();
			for (Object objeto : objetos) {
				float xval = r.nextFloat() * (moscas_radio * 2);
				float yval = r.nextFloat() * (moscas_radio * 2);

				if (xval > moscas_radio) {
					xval += xTam;
				}
				if (yval > moscas_radio) {
					yval += yTam;
				}

				((Mosca) objeto).setPos0(xval, yval);
				((Mosca) objeto).setAnguloRandom();
			}
		}

		private boolean isCircleOnCircles(int n) {
        	
        	for(int i = 0; i < n; i++){
        		if(isCirclesInterseccion((Circulo) objetos[n], (Circulo) objetos[i], 2*circles_radio)){
        			return true;
        		}
        	}			
			return false;
		}

		@Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);

            mHandler.removeCallbacks(mDrawTablet);

        }

        @Override
        public void onOffsetsChanged(float xmOffset, float ymOffset, float xStep, float yStep, int xPixels, int yPixels) {
        	
            mOffset = xmOffset;
            
            if(isVisible() || isPreview()){
            	mHandler.postDelayed(mDrawTablet, 50);//delay black
            }
        }
        
        private void drawTablet() {        	

            final SurfaceHolder holder = getSurfaceHolder();

			//Draw wallpaper image and animation
        	if(isVisible() || isPreview()){        
	            //Cambiar imagen si cambia orientacion de la pantalla
	        	if((use_custom_image && !is_custom_image_loaded) || (!isImgHorizontal && (yTam < xTam)) || (isImgHorizontal && (yTam > xTam))){
	        		setBgImage();
	        	}         		
	            //Calcular nuevos valores para el tiempo
	        	nextTime();
	        	
	        	//Dibujar animacion	
	        	drawAnimation(null, holder);
	        }
        	else{
        		drawBlack(null, holder);
        	}
        	          
	        //Calcular cambios en nuevas posiciones
        	changePositions();
        
            //Llamar proximamente el proximo hilo
            mHandler.removeCallbacks(mDrawTablet);
            if (isVisible() || isPreview()) {
				int RATED = 1;
				mHandler.postDelayed(mDrawTablet, RATED);}
            
        }
        
        @SuppressWarnings("SameParameterValue")
		private void drawAnimation(Canvas c, SurfaceHolder holder){
            try{
            	if(!holder.isCreating()){
		        	try {
		        		c = holder.lockCanvas();
		                if (c != null) {       
		                	c.save();  
		                	c.drawColor(Color.BLACK);
		                	if(is_custom_image_loaded || !use_custom_image){
		                		if(is_scroll){
				                	if(((isImgHorizontal && horizontal_ajuste == B_MOSAICO) || (!isImgHorizontal && vertical_ajuste == B_MOSAICO))){
				                		for(int i = 0; i < xTam*1.25f; i+=background.getWidth()){
				                			for(int j = 0; j < yTam*1.25f; j+=background.getHeight()){				                				
						                		c.drawBitmap(background, i - mOffset * background.getWidth()*0.25f, j - background.getHeight()*0.125f, mPaint);
				                			}
				                		}
				                	}
			                		else{
				                		if(isPreview()){c.drawBitmap(background, - xTam*0.125f, -yTam*0.125f, mPaint);}
				                		else{c.drawBitmap(background, - mOffset * xTam*0.25f, -yTam*0.125f, mPaint);}
				                	}
		                		}
			                    else{
				                	if(((isImgHorizontal && horizontal_ajuste == B_MOSAICO) || (!isImgHorizontal && vertical_ajuste == B_MOSAICO))){
				                		for(int i = 0; i < xTam; i+=background.getWidth()){
				                			for(int j = 0; j < yTam; j+=background.getHeight()){
						                		c.drawBitmap(background, i, j, mPaint);
					                		}
				                		}
				                	}
				                	else{
				                    	c.drawBitmap(background, 0, 0, mPaint);
				                	}
			                    }
		                	}
		                	else{
		                		mPaint.setColor(Color.WHITE);
		                		//mPaint.setAntiAlias(true);//Mejor no cambiar estos valores... :o
		                		//mPaint.setStrokeWidth(0);
		                		mPaint.setTextAlign(Paint.Align.CENTER);
		                		if(tam_imagen == 0){
		                			c.drawText(getResources().getString(R.string.magicpaper_image_null), xTam/2, yTam/2, mPaint);
		                		}
		                		else{
		                			c.drawText(getResources().getString(R.string.magicpaper_image_overload_1), xTam/2, yTam/2, mPaint);
		                			c.drawText(getResources().getString(R.string.magicpaper_image_overload_2), xTam/2, yTam/2 + 20, mPaint);
		                		}
		                	}
		                	c.restore();
		                	c.save();
				            switch(animation){
				            case T_NULL:
				            	//Nada por hacer...
				            	break;
				            case T_CIRCLES:
				            	drawCircles(c);					            		            
				            	break;
				            case T_NIEVE:
				            	drawNieve(c);
				            	break;
				            case T_MOSCAS:
				            	drawMoscas(c);
				            	break;
				            case T_GLOBOS:
				            	drawGlobos(c);
				            	break;
				            case T_CORAZONES:
				            	drawCorazones(c);
				            	break;
				            case T_RAYOSX:
				            	drawRayosX(c);
				            	break;
				            case T_CLOCK:
				            	drawClock(c);
				            	break;
				            }
		                	
		                    c.restore();
		                }
		        	}
		            finally {
		            	if (c != null) {holder.unlockCanvasAndPost(c);}
		            }
	            }
	        }
	    	catch(IllegalArgumentException iae){
	    		//Log.d(this.toString(), "drawAnimation()", iae);        		
	    	} 
        }
        
        private void changePositions(){
	        
        	switch(animation){
            case T_NULL:
            	//Nada por hacer...
            	break;
	        case T_CIRCLES:
	            choquesCirculos();		            
	        	break;
	        case T_NIEVE:
	        	choquesNieve();
	        	break;
            case T_MOSCAS:
            	choquesMoscas();
            	break;
            case T_GLOBOS:
            	choquesGlobos();
            	break;
            case T_CORAZONES:
            	choquesCorazones();
            	break;
            case T_RAYOSX:
            	choquesRayosX();
            	break;
            case T_CLOCK:
            	//choquesClock();
            	break;
	        }
        	
        }

		@SuppressWarnings("SameParameterValue")
		private void drawBlack(Canvas c, SurfaceHolder holder) {
            try{
            	if(!holder.isCreating()){
		            try {
		                c = holder.lockCanvas();
		                if (c != null) {       
		                	c.save();  
		                	
		                	//Draw background  
		                	c.drawColor(Color.BLACK);
		                	
		                    c.restore();
		                }
		            } 
		            finally {
		            	if (c != null) {holder.unlockCanvasAndPost(c);}
		            }
            	}
            }
        	catch(IllegalArgumentException iae){
        		//Log.d(getPackageCodePath(), "drawBlack()", iae);
        	}
		}
		
		@SuppressWarnings("IntegerDivisionInFloatingPointContext")
		private void drawRayosX(Canvas c) {
			//Draw RayosX
			int largo = ((RayosX) objetos[0]).lente.getWidth();
			
			float xpos;
			float ypos;
			
			if(((RayosX) objetos[0]).isManual()){
				xpos = ((RayosX) objetos[0]).getxPos0();
				ypos = ((RayosX) objetos[0]).getyPos0();
			}
			else{
				xpos = ((RayosX) objetos[0]).getxPos(t_rapidez);
				ypos = ((RayosX) objetos[0]).getyPos(t_rapidez);
			}
			
			//Redimensionar imagen si es necesario...
            if(largo != (int)(2*rayosx_radio)){
                Drawable lente = getResources().getDrawable(R.drawable.lente);
                ((RayosX) objetos[0]).lente = Bitmap.createBitmap((int)(2*rayosx_radio), (int)(2*rayosx_radio), Bitmap.Config.ALPHA_8);
                lente.setBounds(0, 0, (int)(2*rayosx_radio), (int)(2*rayosx_radio));
                lente.draw(new Canvas(((RayosX) objetos[0]).lente));            	
            }
            
            //Dibujar lente
			mPaint.setColor(Color.BLACK);
			c.drawRect(0, 0, xTam, ypos - largo/2, mPaint);
			c.drawRect(0, ypos - largo/2, xpos - largo/2, ypos + largo/2, mPaint);
			c.drawRect(xpos + largo/2, ypos - largo/2, xTam, ypos + largo/2, mPaint);
			c.drawRect(0, ypos + largo/2, xTam, yTam, mPaint);
			c.drawLine(xpos, ypos - largo/2, xpos, ypos + largo/2, mPaint);
			c.drawLine(xpos - largo/2, ypos, xpos + largo/2, ypos, mPaint);
            c.drawBitmap(((RayosX) objetos[0]).lente, xpos - largo/2, ypos - largo/2, mPaint);              
		}
		
		private void drawCircles(Canvas c) {
			//Draw Circulos
			for (Object objeto : objetos) {
				mPaint.setColor(((Circulo) objeto).getColor());
				c.drawCircle(((Circulo) objeto).getxPos(t_rapidez), ((Circulo) objeto).getyPos(t_rapidez), circles_radio, mPaint);
			}
		}

		private void drawNieve(Canvas c) {    	            

            //mPaint.setColor(Color.WHITE);
            
			//Draw Circulos
			for (Object objeto : objetos) {
				float xval = ((Nieve) objeto).getxPos(t_rapidez) - ((Nieve) objeto).getRadio() * 2 - 4;
				float yval = ((Nieve) objeto).getyPos(t_rapidez) - ((Nieve) objeto).getRadio() * 2 - 4;
				c.drawBitmap(Nieve.copo_nieve, null, new RectF(xval, yval, xval + ((Nieve) objeto).getRadio() * 2 + 4,
						yval + ((Nieve) objeto).getRadio() * 2 + 4), mPaint);
			}
            
		}		
		
		private void drawGlobos(Canvas c) {
            
            float xval;
            float yval;

			for (Object objeto : objetos) {
				xval = ((Globo) objeto).getxPos(t_rapidez);
				yval = ((Globo) objeto).getyPos(t_rapidez);

				switch (((Globo) objeto).getColor()) {
					case Color.YELLOW:
						c.drawBitmap(Globo.globo_amarillo, null, new RectF(xval, yval, xval + ((Globo) objeto).getRadio() * 2 + 4,
								yval + 3 * (((Globo) objeto).getRadio() * 2 + 4)), mPaint);
						break;
					case Color.BLUE:
						c.drawBitmap(Globo.globo_azul, null, new RectF(xval, yval, xval + ((Globo) objeto).getRadio() * 2 + 4,
								yval + 3 * (((Globo) objeto).getRadio() * 2 + 4)), mPaint);
						break;
					case Color.RED:
						c.drawBitmap(Globo.globo_rojo, null, new RectF(xval, yval, xval + ((Globo) objeto).getRadio() * 2 + 4,
								yval + 3 * (((Globo) objeto).getRadio() * 2 + 4)), mPaint);
						break;
					case Color.GREEN:
						c.drawBitmap(Globo.globo_verde, null, new RectF(xval, yval, xval + ((Globo) objeto).getRadio() * 2 + 4,
								yval + 3 * (((Globo) objeto).getRadio() * 2 + 4)), mPaint);
						break;
				}
			}
		}		
		
		private void drawCorazones(Canvas c) {
			float xval;
            float yval;
			for (Object objeto : objetos) {
				xval = ((Globo) objeto).getxPos(t_rapidez);
				yval = ((Globo) objeto).getyPos(t_rapidez);
				c.drawBitmap(Globo.globo, null, new RectF(xval, yval, xval + ((Globo) objeto).getRadio() * 2 + 4,
						yval + (((Globo) objeto).getRadio() * 2 + 4)), mPaint);
			}
		}
		
		private void drawMoscas(Canvas c) {	
            
			float xval;
			float yval;
			
			RectF rectf = new RectF();			
			
            Canvas canvita = new Canvas(Mosca.b);   
            
			//Draw Moscas
			for (Object objeto : objetos) {
				canvita.save();

				canvita.rotate(((Mosca) objeto).getAngulo(), (int) moscas_radio, (int) moscas_radio);
				Mosca.b.eraseColor(Color.TRANSPARENT);

				rectf.set(0, 0, moscas_radio * 2, moscas_radio * 2);
				if (((Mosca) objeto).isMatarMosca()) {
					canvita.drawBitmap(Mosca.mosca0, null, rectf, mPaint);

					xval = ((Mosca) objeto).getxPos_Muerta();
					yval = ((Mosca) objeto).getyPos_Muerta();
					((Mosca) objeto).aleteo();
				} else {
					if (((Mosca) objeto).aleteo()) {
						canvita.drawBitmap(Mosca.mosca1, null, rectf, mPaint);
					} else {
						canvita.drawBitmap(Mosca.mosca2, null, rectf, mPaint);
					}

					xval = ((Mosca) objeto).getxPos(t_rapidez);
					yval = ((Mosca) objeto).getyPos(t_rapidez);
				}
				rectf.set(xval - moscas_radio * 2, yval - moscas_radio * 2, xval, yval);
				c.drawBitmap(Mosca.b, null, rectf, mPaint);

				canvita.restore();
			}
		}	

		private void drawClock(Canvas c) {
			float xval;
			float yval;
			
			float clock_radio = Math.min(xTam/3, yTam/3);
			c.translate(xTam/2, yTam/2);
			
			mPaint.setAntiAlias(true);			
			mPaint.setTextAlign(Align.CENTER);
			
			//Numeros
			for(int i = 1; i <= 12; i++){
				xval = (float) (clock_radio*Math.cos(Math.PI/6*i - Math.PI/2));
				yval = (float) (clock_radio*Math.sin(Math.PI/6*i - Math.PI/2));
				mPaint.setColor(Color.BLACK);
				mPaint.setTextSize(clock_radio/5 + 4);
				c.drawText(String.valueOf(i), xval, yval + 2, mPaint);
				mPaint.setColor(Color.WHITE);
				mPaint.setTextSize(clock_radio/5);
				c.drawText(String.valueOf(i), xval, yval, mPaint);
			}
			
			Calendar calendario = Calendar.getInstance();
			int hora = calendario.get(Calendar.HOUR);
			int minuto = calendario.get(Calendar.MINUTE);
			int segundo = calendario.get(Calendar.SECOND);
			
			mPaint.setStyle(Style.FILL);
			
			//Draw
			c.translate(0, -clock_radio/10);
			
			//Horas
			mPaint.setColor(Color.BLACK);
			mPaint.setStrokeWidth(9);
			c.drawLine(0, 0, (float) ((clock_radio + 2)*Math.cos(Math.PI/6*hora - Math.PI/2)*2/3), 
					(float) ((clock_radio + 2)*Math.sin(Math.PI/6*hora - Math.PI/2)*2/3), mPaint);
			
			mPaint.setColor(Color.WHITE);
			mPaint.setStrokeWidth(7);
			c.drawLine(0, 0, (float) (clock_radio*Math.cos(Math.PI/6*hora - Math.PI/2)*2/3), 
					(float) (clock_radio*Math.sin(Math.PI/6*hora - Math.PI/2)*2/3), mPaint);
			//Minutos
			mPaint.setColor(Color.BLACK);
			mPaint.setStrokeWidth(5);
			c.drawLine(0, 0, (float) ((clock_radio + 2)*Math.cos(Math.PI/30*minuto - Math.PI/2)), 
					(float) ((clock_radio + 2)*Math.sin(Math.PI/30*minuto - Math.PI/2)), mPaint);
			
			mPaint.setColor(Color.WHITE);
			mPaint.setStrokeWidth(3);
			c.drawLine(0, 0, (float) (clock_radio*Math.cos(Math.PI/30*minuto - Math.PI/2)), 
					(float) (clock_radio*Math.sin(Math.PI/30*minuto - Math.PI/2)), mPaint);
			//Segundos
			mPaint.setColor(Color.BLACK);
			mPaint.setStrokeWidth(3);
			c.drawLine((float) ((clock_radio + 2)*Math.cos(Math.PI/30*segundo + Math.PI/2)/3), 
					(float) ((clock_radio + 2)*Math.sin(Math.PI/30*segundo + Math.PI/2)/3), 
					(float) ((clock_radio + 2)*Math.cos(Math.PI/30*segundo - Math.PI/2)), 
					(float) ((clock_radio + 2)*Math.sin(Math.PI/30*segundo - Math.PI/2)), mPaint);
			c.drawCircle(0, 0, 7, mPaint);
			
			mPaint.setColor(Color.WHITE);
			mPaint.setStrokeWidth(1);
			c.drawLine((float) (clock_radio*Math.cos(Math.PI/30*segundo + Math.PI/2)/3), 
					(float) (clock_radio*Math.sin(Math.PI/30*segundo + Math.PI/2)/3), 
					(float) (clock_radio*Math.cos(Math.PI/30*segundo - Math.PI/2)), 
					(float) (clock_radio*Math.sin(Math.PI/30*segundo - Math.PI/2)), mPaint);
			c.drawCircle(0, 0, 5, mPaint);
		}
		
		private void nextTime() {
    		//86.400.000 ms en un dia... :o ...Para reducir un poco el uso de memoria... ;)
    		if(t > 0x60000){//0x10000
    			switch(animation){
	            case T_NULL:
	            	//Nada por hacer...
	            	break;
    			case T_CIRCLES:
					for (Object objeto4 : objetos) {
						((Circulo) objeto4).setPos0(((Circulo) objeto4).getxPos(t_rapidez), ((Circulo) objeto4).getyPos(t_rapidez));
					}
    				break;
    			case T_NIEVE:
					for (Object objeto3 : objetos) {
						((Nieve) objeto3).setPos0(((Nieve) objeto3).getxPos(t_rapidez), ((Nieve) objeto3).getyPos(t_rapidez));
					}
    				break;
                case T_MOSCAS:
					for (Object objeto2 : objetos) {
						((Mosca) objeto2).setPos0(((Mosca) objeto2).getxPos(t_rapidez), ((Mosca) objeto2).getyPos(t_rapidez));
					}
                	break;
    			case T_GLOBOS:
					for (Object objeto1 : objetos) {
						((Globo) objeto1).setPos0(((Globo) objeto1).getxPos(t_rapidez), ((Globo) objeto1).getyPos(t_rapidez));
					}
    				break;
    			case T_CORAZONES:
					for (Object objeto : objetos) {
						((Globo) objeto).setPos0(((Globo) objeto).getxPos(t_rapidez), ((Globo) objeto).getyPos(t_rapidez));
					}
    				break;
    			case T_RAYOSX:
    				if(!((RayosX) objetos[0]).isManual()){
    					((RayosX) objetos[0]).setPos0(((RayosX) objetos[0]).getxPos(t_rapidez), ((RayosX) objetos[0]).getyPos(t_rapidez));
    				}    				
    				break;
    			case T_CLOCK:
    				 				
    				break;
    			}
    			mStartTime = SystemClock.elapsedRealtime();
    			tpas = tpas - t;
    			t = 0;
    			t_rapidez = 0;
    		}
    		else{
	            tpas = t;
	            t = (SystemClock.elapsedRealtime() - mStartTime); 
	            t_rapidez = t*rapidez;
            }
		}

		/**
		 * @param f  Factor
		 * @param a, b Circulos a evaluar
		 * 
		 * @return true si hay interseccion o false en caso contrario
		 */
		private Boolean isCirclesInterseccion(Circulo a, Circulo b, float f){			
			
			float dx = a.getxPos(t_rapidez) - b.getxPos(t_rapidez);
			float dy = a.getyPos(t_rapidez) - b.getyPos(t_rapidez);
			return Math.hypot(dx, dy) <= 2 * f;

		}
		
		private Boolean isMoscasInterseccion(Mosca a, Mosca b, float f){			
			
			if(!a.isMatarMosca() && !b.isMatarMosca()){
				float dx = a.getxPos(t_rapidez) - b.getxPos(t_rapidez);
				float dy = a.getyPos(t_rapidez) - b.getyPos(t_rapidez);
				return Math.hypot(dx, dy) <= 2 * f;
			}
			
			return false;			
		}
		
		/**
		 * @param a, b indices en el array de circulos a ejecutar en el choque
		 */
		private void ejecutarChoqueCirculo(int a, int b){			
			
			//Vector normal
			float dx = ((Circulo) objetos[a]).getxPos(tpas*rapidez) - ((Circulo) objetos[b]).getxPos(tpas*rapidez);
			float dy = ((Circulo) objetos[a]).getyPos(tpas*rapidez) - ((Circulo) objetos[b]).getyPos(tpas*rapidez);
			float c = (float) Math.hypot(dx, dy);		
			float cux = (((Circulo) objetos[a]).getxPos(tpas*rapidez) - ((Circulo) objetos[b]).getxPos(tpas*rapidez)) / c;
			float cuy = (((Circulo) objetos[a]).getyPos(tpas*rapidez) - ((Circulo) objetos[b]).getyPos(tpas*rapidez)) / c;		
			
			//Calculos finales de rapides
			float navx = cux*(((Circulo) objetos[b]).getxDir()*cux + ((Circulo) objetos[b]).getyDir()*cuy) - cuy*(((Circulo) objetos[a]).getyDir()*cux - ((Circulo) objetos[a]).getxDir()*cuy);
			float navy = cuy*(((Circulo) objetos[b]).getxDir()*cux + ((Circulo) objetos[b]).getyDir()*cuy) + cux*(((Circulo) objetos[a]).getyDir()*cux - ((Circulo) objetos[a]).getxDir()*cuy);
			float nbvx = cux*(((Circulo) objetos[a]).getxDir()*cux + ((Circulo) objetos[a]).getyDir()*cuy) - cuy*(((Circulo) objetos[b]).getyDir()*cux - ((Circulo) objetos[b]).getxDir()*cuy);
			float nbvy = cuy*(((Circulo) objetos[a]).getxDir()*cux + ((Circulo) objetos[a]).getyDir()*cuy) + cux*(((Circulo) objetos[b]).getyDir()*cux - ((Circulo) objetos[b]).getxDir()*cuy);						
			
			//Almacenamiento de posiciones iniciales
			((Circulo) objetos[a]).setPos0((((Circulo) objetos[a]).getxDir() - navx)*tpas*rapidez + ((Circulo) objetos[a]).getxPos0(), (((Circulo) objetos[a]).getyDir() - navy)*tpas*rapidez + ((Circulo) objetos[a]).getyPos0());
			((Circulo) objetos[b]).setPos0((((Circulo) objetos[b]).getxDir() - nbvx)*tpas*rapidez + ((Circulo) objetos[b]).getxPos0(), (((Circulo) objetos[b]).getyDir() - nbvy)*tpas*rapidez + ((Circulo) objetos[b]).getyPos0());
			
			//Almacenamiento de direcciones
			((Circulo) objetos[a]).setxyDir(navx, navy);
			((Circulo) objetos[b]).setxyDir(nbvx, nbvy);	
		}
		
		/**
		 * Calcula y asigna nuevas posiciones para cada uno de los circulos en el tablero<br>
		 * 
		 * ij  ij  ij <br>
		 * 00- 01* 02*<br>
		 * 10- 11- 12*<br>
		 * 20- 21- 22-<br>
		 */	
		private void choquesCirculos(){
			
			for(int i = 0; i < objetos.length; i++){
				for(int j = i + 1; j < objetos.length; j++){
					if(isCirclesInterseccion(((Circulo) objetos[i]), ((Circulo) objetos[j]), circles_radio)){ejecutarChoqueCirculo(i, j);}
				}
				
				if(((((Circulo) objetos[i]).getxPos(t_rapidez) >= xTam - circles_radio) && (((Circulo) objetos[i]).getxDir() > 0)) || 
						((((Circulo) objetos[i]).getxPos(t_rapidez) <= circles_radio) && (((Circulo) objetos[i]).getxDir() < 0))){					
					
					((Circulo) objetos[i]).setPos0(2*((Circulo) objetos[i]).getxDir()*t_rapidez + ((Circulo) objetos[i]).getxPos0(), ((Circulo) objetos[i]).getyPos0());
					((Circulo) objetos[i]).setxyDir(-((Circulo) objetos[i]).getxDir(), ((Circulo) objetos[i]).getyDir());								
				
				}
				else if(((((Circulo) objetos[i]).getyPos(t_rapidez) >= yTam - circles_radio) && (((Circulo) objetos[i]).getyDir() > 0)) || 
						((((Circulo) objetos[i]).getyPos(t_rapidez) <= circles_radio) && (((Circulo) objetos[i]).getyDir() < 0))){										
				
					((Circulo) objetos[i]).setPos0(((Circulo) objetos[i]).getxPos0(), 2*((Circulo) objetos[i]).getyDir()*t_rapidez + ((Circulo) objetos[i]).getyPos0());					
					((Circulo) objetos[i]).setxyDir(((Circulo) objetos[i]).getxDir(), -((Circulo) objetos[i]).getyDir());					
				
				}
			}
		}		
		
		private void choquesRayosX(){		
			if(!((RayosX) objetos[0]).isManual()){
				if(((((RayosX) objetos[0]).getxPos(t_rapidez) >= xTam - rayosx_radio) && (((RayosX) objetos[0]).getxDir() > 0)) || 
						((((RayosX) objetos[0]).getxPos(t_rapidez) <= rayosx_radio) && (((RayosX) objetos[0]).getxDir() < 0))){					
					((RayosX) objetos[0]).setPos0(2*((RayosX) objetos[0]).getxDir()*t_rapidez + ((RayosX) objetos[0]).getxPos0(), ((RayosX) objetos[0]).getyPos0());
					((RayosX) objetos[0]).setxyDir(-((RayosX) objetos[0]).getxDir(), ((RayosX) objetos[0]).getyDir());				
				}
				else if(((((RayosX) objetos[0]).getyPos(t_rapidez) >= yTam - rayosx_radio) && (((RayosX) objetos[0]).getyDir() > 0)) || 
						((((RayosX) objetos[0]).getyPos(t_rapidez) <= rayosx_radio) && (((RayosX) objetos[0]).getyDir() < 0))){	
					((RayosX) objetos[0]).setPos0(((RayosX) objetos[0]).getxPos0(), 2*((RayosX) objetos[0]).getyDir()*t_rapidez + ((RayosX) objetos[0]).getyPos0());					
					((RayosX) objetos[0]).setxyDir(((RayosX) objetos[0]).getxDir(), -((RayosX) objetos[0]).getyDir());			
				}
			}
		}
		
		/**
		 * Calcula y asigna nuevas posiciones para cada uno de los circulos en el tablero<br>
		 * 
		 * ij  ij  ij <br>
		 * 00- 01* 02*<br>
		 * 10- 11- 12*<br>
		 * 20- 21- 22-<br>
		 */	
		private void choquesNieve(){
			for (Object objeto : objetos) {
				if (((Nieve) objeto).getxPos(t_rapidez) > xTam + ((Nieve) objeto).getRadio() * 2 + 4) {
					((Nieve) objeto).setPos0(((Nieve) objeto).getxPos0() - xTam - ((Nieve) objeto).getRadio() * 2 - 4, ((Nieve) objeto).getyPos0());
				} else if (((Nieve) objeto).getxPos(t_rapidez) < 0) {
					((Nieve) objeto).setPos0(((Nieve) objeto).getxPos0() + xTam + ((Nieve) objeto).getRadio() * 2 + 4, ((Nieve) objeto).getyPos0());
				} else if (((Nieve) objeto).getyPos(t_rapidez) > yTam + ((Nieve) objeto).getRadio() * 2 + 4) {
					((Nieve) objeto).setPos0(((Nieve) objeto).getxPos0(), ((Nieve) objeto).getyPos0() - yTam - ((Nieve) objeto).getRadio() * 2 - 4);
				}
			}
		}
		
		private void choquesGlobos(){
			for (Object objeto : objetos) {
				if (((Globo) objeto).getxPos(t_rapidez) > xTam) {
					((Globo) objeto).setPos0(((Globo) objeto).getxPos0() - xTam - (((Globo) objeto).getRadio() * 2 + 4), ((Globo) objeto).getyPos0());
				} else if (((Globo) objeto).getxPos(t_rapidez) < -(((Globo) objeto).getRadio() * 2 + 4)) {
					((Globo) objeto).setPos0(((Globo) objeto).getxPos0() + xTam + ((Globo) objeto).getRadio() * 2 + 4, ((Globo) objeto).getyPos0());
				} else if (((Globo) objeto).getyPos(t_rapidez) < -3 * (((Globo) objeto).getRadio() * 2 + 4)) {
					((Globo) objeto).setPos0(((Globo) objeto).getxPos0(), ((Globo) objeto).getyPos0() + yTam + 3 * (((Globo) objeto).getRadio() * 2 + 4));
				}
			}
		}
		
		private void choquesCorazones(){
			for (Object objeto : objetos) {
				if (((Globo) objeto).getxPos(t_rapidez) > xTam) {
					((Globo) objeto).setPos0(((Globo) objeto).getxPos0() - xTam - (((Globo) objeto).getRadio() * 2 + 4), ((Globo) objeto).getyPos0());
				} else if (((Globo) objeto).getxPos(t_rapidez) < -(((Globo) objeto).getRadio() * 2 + 4)) {
					((Globo) objeto).setPos0(((Globo) objeto).getxPos0() + xTam + ((Globo) objeto).getRadio() * 2 + 4, ((Globo) objeto).getyPos0());
				} else if (((Globo) objeto).getyPos(t_rapidez) < -(((Globo) objeto).getRadio() * 2 + 4)) {
					((Globo) objeto).setPos0(((Globo) objeto).getxPos0(), ((Globo) objeto).getyPos0() + yTam + (((Globo) objeto).getRadio() * 2 + 4));
				}
			}
		}

		private void initMosca(Mosca mosca){
			Random r = new Random();
            float xval = 0;
            float yval = 0;
            
			switch(r.nextInt(4)){
			case 0://Arriba
	            xval = r.nextFloat()*(xTam + moscas_radio*2);
	            yval = r.nextFloat()*(moscas_radio);
				break;
			case 1://Abajo
	            xval = r.nextFloat()*(xTam + moscas_radio*2);
	            yval = r.nextFloat()*(moscas_radio) + yTam + moscas_radio;
				break;
			case 2://Derecha
	            xval = r.nextFloat()*(moscas_radio) + xTam + moscas_radio;
	            yval = r.nextFloat()*(yTam + moscas_radio*2);
				break;
			case 3://Izquierda
	            xval = r.nextFloat()*(moscas_radio);
	            yval = r.nextFloat()*(yTam + moscas_radio*2);
				break;
			}
			
            mosca.setAnguloRandom();
            mosca.setPos0(xval - mosca.getxDir()*t_rapidez, yval - mosca.getyDir()*t_rapidez);
           
		}
		
		private void choquesMoscas(){
            
			for(int i = 0; i < objetos.length; i++){
				if(moscas_pelean){
					for(int j = i + 1; j < objetos.length; j++){
						if(isMoscasInterseccion(((Mosca) objetos[i]), ((Mosca) objetos[j]), moscas_radio)){
							float xval = ((Mosca) objetos[i]).getxPos(t_rapidez);
							float yval = ((Mosca) objetos[i]).getyPos(t_rapidez);
							((Mosca) objetos[i]).setAnguloRandom();
							((Mosca) objetos[i]).setPos0(
									xval - ((Mosca) objetos[i]).getxDir()*t_rapidez, 
									yval - ((Mosca) objetos[i]).getyDir()*t_rapidez);
							
							xval = ((Mosca) objetos[j]).getxPos(t_rapidez);
							yval = ((Mosca) objetos[j]).getyPos(t_rapidez);
							((Mosca) objetos[j]).setAnguloRandom();
							((Mosca) objetos[j]).setPos0(
									xval - ((Mosca) objetos[j]).getxDir()*t_rapidez, 
									yval - ((Mosca) objetos[j]).getyDir()*t_rapidez);
						}
					}
				}

				switch (((Mosca) objetos[i]).getEstado()) {
					case Mosca.MOSCA_MUERTA:

						break;
					case Mosca.MOSCA_INICIO:
						initMosca((Mosca) objetos[i]);

						((Mosca) objetos[i]).setOut(false);
						break;
					case Mosca.MOSCA_SALIO:
						initMosca((Mosca) objetos[i]);

						((Mosca) objetos[i]).setOut(false);
						break;
					case Mosca.MOSCA_ENTRO:
						if (((Mosca) objetos[i]).getxPos(t_rapidez) > xTam + moscas_radio * 2 ||
								((Mosca) objetos[i]).getxPos(t_rapidez) < 0 ||
								((Mosca) objetos[i]).getyPos(t_rapidez) > yTam + moscas_radio * 2 ||
								((Mosca) objetos[i]).getyPos(t_rapidez) < 0) {
							((Mosca) objetos[i]).setOut(true);
						}
						break;
				}
			}
		}
    }
}
