/*

  A Magic Wallpaper
  		Copyright (C) 2019  Jorge Eliécer Sanabria Hernández

  		This program is free software: you can redistribute it and/or modify
  		it under the terms of the GNU General Public License as published by
  		the Free Software Foundation, either version 3 of the License, or
  		(at your option) any later version.

  		This program is distributed in the hope that it will be useful,
  		but WITHOUT ANY WARRANTY; without even the implied warranty of
  		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  		GNU General Public License for more details.

  		You should have received a copy of the GNU General Public License
  		along with this program.  If not, see <http://www.gnu.org/licenses/>.

  		jesanabriah@gmail.com

 */

package com.alicesmagic.android.magicpaper;

import android.Manifest;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MagicPaperLauncher extends Activity {

	private final int REQUEST_CODE = 1;

    private static final int REQ_CODE = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQ_CODE);
                }

            } else {

                // No explanation needed, we can request the permission.

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQ_CODE);
                }

                // REQ_CODE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else{
            startWallpaperApp();
        }
	}

	@SuppressWarnings("ConstantConditions")
    private void startWallpaperApp(){
        Intent i = new Intent();

        if (Build.VERSION.SDK_INT > 15) {
            i.setAction(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);

            String p = MagicPaper.class.getPackage().getName();
            String c = MagicPaper.class.getCanonicalName();
            i.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(p, c));
        } else {
            i.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
        }
        startActivityForResult(i, REQUEST_CODE);
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent)	{
		super.onActivityResult(requestCode,resultCode, intent);

		if (requestCode == REQUEST_CODE) {finish();}
	}

    @SuppressWarnings("NullableProblems")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
/*        switch (requestCode) {
            case REQ_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                //return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }*/
        startWallpaperApp();
    }
}
