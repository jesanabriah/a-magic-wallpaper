/*

  A Magic Wallpaper
  		Copyright (C) 2019  Jorge Eliécer Sanabria Hernández

  		This program is free software: you can redistribute it and/or modify
  		it under the terms of the GNU General Public License as published by
  		the Free Software Foundation, either version 3 of the License, or
  		(at your option) any later version.

  		This program is distributed in the hope that it will be useful,
  		but WITHOUT ANY WARRANTY; without even the implied warranty of
  		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  		GNU General Public License for more details.

  		You should have received a copy of the GNU General Public License
  		along with this program.  If not, see <http://www.gnu.org/licenses/>.

  		jesanabriah@gmail.com

 */

package com.alicesmagic.android.magicpaper;

import java.util.Random;

import android.graphics.Color;

class Circulo {

	private int color;
	
	//Posicion inicial
	private float xPos0 = 0;
	private float yPos0 = 0;
	
	//Direccion
	private float xDir;
	private float yDir;
	
	Circulo() {
				
		//Valor por defecto para la direccion
		Random r = new Random();
		setAngulo((float)r.nextGaussian() * 2 * (float)Math.PI);		
		setRandomColor();
	}

	Boolean isPointOnCircle(float x, float y, float t, float radio) {
		
		float dx = getxPos(t) - x;
		float dy = getyPos(t) - y;
		float distancia = (float) Math.hypot(dx, dy);

		return distancia < radio;

	}

	void setPos0(float xPos0, float yPos0) {
		setxPos0(xPos0);
		setyPos0(yPos0);
	}
	
	/**
	 * @return the xPos0
	 */
	float getxPos0() {
		return xPos0;
	}

	/**
	 * @param xPos0 the xPos0 to set
	 */
	private void setxPos0(float xPos0) {
		this.xPos0 = xPos0;
	}

	/**
	 * @return the yPos0
	 */
	float getyPos0() {
		return yPos0;
	}

	/**
	 * @param yPos0 the yPos to set
	 */
	private void setyPos0(float yPos0) {
		this.yPos0 = yPos0;
	}
	
	/**
	 * @return the xPos
	 */
	float getxPos(float t) {
		return (getxDir()*t) + xPos0;
	}

	/**
	 * @return the yPos
	 */
	float getyPos(float t) {
		return (getyDir()*t) + yPos0;
	}

	/**
	 * @return the xDir
	 */
	float getxDir() {
		return xDir;
	}

	/**
	 * @return the yDir
	 */
	float getyDir() {
		return yDir;
	}

	/**
	 * @param angulo the angulo to set
	 */
	private void setAngulo(float angulo) {
		this.xDir = (float) (Math.cos(angulo)/16);
		this.yDir = (float) (Math.sin(angulo)/16);
	}

	void setxyDir(float xDir, float yDir) {
		this.xDir = xDir;
		this.yDir = yDir;
	}	
	
	/**
	 * @return the color
	 */
	int getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	private void setColor(int color) {
		this.color = color;
	}

	/**
	 */
	public void setRandomColor() {	
		Random r = new Random();
		int[] colores = new int[7];
		colores[0] = Color.RED;
		colores[1] = Color.BLUE;
		colores[2] = Color.CYAN;
		colores[3] = Color.WHITE;
		colores[4] = Color.MAGENTA;
		colores[5] = Color.GREEN;
		colores[6] = Color.YELLOW;
		
		//Valor para el color
		//setColor(Color.WHITE);
		setColor(colores[r.nextInt(7)]);
	}
	
// --Commented out by Inspection START (21/01/19 06:32 PM):
//	/**
//	 * @return the isFill
//	 */
//	public boolean isFill() {
//		return isFill;
//	}
// --Commented out by Inspection STOP (21/01/19 06:32 PM)

// --Commented out by Inspection START (21/01/19 06:41 PM):
//	/**
//	 * @param isFill the isFill to set
//	 */
//	public void setFill(boolean isFill) {
//		//private String vletra = "J";
//		boolean isFill1 = isFill;
//	}
// --Commented out by Inspection STOP (21/01/19 06:41 PM)
}
