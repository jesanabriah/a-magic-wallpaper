/*

  A Magic Wallpaper
  		Copyright (C) 2019  Jorge Eliécer Sanabria Hernández

  		This program is free software: you can redistribute it and/or modify
  		it under the terms of the GNU General Public License as published by
  		the Free Software Foundation, either version 3 of the License, or
  		(at your option) any later version.

  		This program is distributed in the hope that it will be useful,
  		but WITHOUT ANY WARRANTY; without even the implied warranty of
  		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  		GNU General Public License for more details.

  		You should have received a copy of the GNU General Public License
  		along with this program.  If not, see <http://www.gnu.org/licenses/>.

  		jesanabriah@gmail.com

 */

package com.alicesmagic.android.magicpaper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

final class SeekBarPreference extends DialogPreference implements OnSeekBarChangeListener {

    // Namespaces to read attributes
    private static final String PREFERENCE_NS = "http://schemas.android.com/apk/res/com.alicesmagic.android.magicpaper";
    private static final String ANDROID_NS = "http://schemas.android.com/apk/res/android";

    // Attribute names
    private static final String ATTR_DEFAULT_VALUE = "defaultValue";
    private static final String ATTR_MIN_VALUE = "minValue";
    private static final String ATTR_MAX_VALUE = "maxValue";
    private static final String ATTR_SUFIJO = "sufijo";

    // Default values for defaults
    private static final int DEFAULT_CURRENT_VALUE = 50;
    private static final int DEFAULT_MIN_VALUE = 1;
    private static final int DEFAULT_MAX_VALUE = 100;
    private static final String DEFAULT_SUFIJO = " ";

    // Real defaults
    private final int mDefaultValue;
    private final int mMaxValue;
    private final int mMinValue;
    private final String mSufijo;
    
    // Current value
    private int mCurrentValue;

    private TextView mValueText;

    public SeekBarPreference(Context context, AttributeSet attrs) {
	    super(context, attrs);

		// Read parameters from attributes
		mMinValue = attrs.getAttributeIntValue(PREFERENCE_NS, ATTR_MIN_VALUE, DEFAULT_MIN_VALUE);
		mMaxValue = attrs.getAttributeIntValue(PREFERENCE_NS, ATTR_MAX_VALUE, DEFAULT_MAX_VALUE);
		mDefaultValue = attrs.getAttributeIntValue(ANDROID_NS, ATTR_DEFAULT_VALUE, DEFAULT_CURRENT_VALUE);
		 
		String mSufijo_temp = attrs.getAttributeValue(PREFERENCE_NS, ATTR_SUFIJO);
		if(mSufijo_temp == null){
			mSufijo = DEFAULT_SUFIJO;
		}
		else{
			mSufijo = mSufijo_temp;
		}
	}

    @SuppressLint("SetTextI18n")
    @Override
    protected View onCreateDialogView() {
		// Get current value from preferences
		mCurrentValue = getPersistedInt(mDefaultValue);
	
		// Inflate layout
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.preference_seekbar, null);
	
		// Setup minimum and maximum text labels
		((TextView) view.findViewById(R.id.min_value)).setText(mMinValue + mSufijo);
		((TextView) view.findViewById(R.id.max_value)).setText(mMaxValue + mSufijo);
	
		// Setup SeekBar
        // View elements
        SeekBar mSeekBar = view.findViewById(R.id.seek_bar);
		mSeekBar.setMax(mMaxValue - mMinValue);
		mSeekBar.setProgress(mCurrentValue - mMinValue);
		mSeekBar.setOnSeekBarChangeListener(this);
	
		// Setup text label for current value
		mValueText = view.findViewById(R.id.current_value);
		mValueText.setText(mCurrentValue + mSufijo);
	
		return view;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
	    super.onDialogClosed(positiveResult);

		// Return if change was cancelled
		if (!positiveResult) {
		    return;
		}
		
		// Persist current value if needed
		if (shouldPersist()) {
		    persistInt(mCurrentValue);
		}
	
		// Notify activity about changes (to update preference summary line)
		notifyChanged();
    }

    @Override
    public CharSequence getSummary() {
		// Format summary string with current value
		String summary = super.getSummary().toString();
		int value = getPersistedInt(mDefaultValue);
		return String.format(summary, value);
    }
    
    @SuppressLint("SetTextI18n")
    public void onProgressChanged(SeekBar seek, int value, boolean fromTouch) {
		// Update current value
		mCurrentValue = value + mMinValue;
		// Update label with current value
		mValueText.setText(mCurrentValue + mSufijo);
    }

    public void onStartTrackingTouch(SeekBar seek) {
	// Not used
    }

    public void onStopTrackingTouch(SeekBar seek) {
	// Not used
    }
}