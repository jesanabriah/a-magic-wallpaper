/*

  A Magic Wallpaper
  		Copyright (C) 2019  Jorge Eliécer Sanabria Hernández

  		This program is free software: you can redistribute it and/or modify
  		it under the terms of the GNU General Public License as published by
  		the Free Software Foundation, either version 3 of the License, or
  		(at your option) any later version.

  		This program is distributed in the hope that it will be useful,
  		but WITHOUT ANY WARRANTY; without even the implied warranty of
  		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  		GNU General Public License for more details.

  		You should have received a copy of the GNU General Public License
  		along with this program.  If not, see <http://www.gnu.org/licenses/>.

  		jesanabriah@gmail.com

 */

package com.alicesmagic.android.magicpaper;

import java.util.Random;

import android.graphics.Bitmap;

class RayosX {
	
	//Posicion inicial
	private float xPos0 = 0;
	private float yPos0 = 0;
	
	//Direccion
	private float xDir;
	private float yDir;
	
	private boolean manual = false;
	
	public Bitmap lente = null;
	
	public RayosX() {				
		//Valor por defecto para la direccion
		Random r = new Random();
		setAngulo((float)r.nextGaussian() * 2 * (float)Math.PI);	
		setManual(false);
	}

// --Commented out by Inspection START (21/01/19 07:11 PM):
//	public Boolean isPointOnCircle(float x, float y, float t, float radio) {
//
//		float dx = getxPos(t) - x;
//		float dy = getyPos(t) - y;
//		float distancia = (float) Math.hypot(dx, dy);
//
//		return distancia < radio;
//
//	}
// --Commented out by Inspection STOP (21/01/19 07:11 PM)

	public void setPos0(float xPos0, float yPos0) {
		setxPos0(xPos0);
		setyPos0(yPos0);
	}
	
	/**
	 * @return the xPos0
	 */
	public float getxPos0() {
		return xPos0;
	}

	/**
	 * @param xPos0 the xPos0 to set
	 */
	private void setxPos0(float xPos0) {
		this.xPos0 = xPos0;
	}

	/**
	 * @return the yPos0
	 */
	public float getyPos0() {
		return yPos0;
	}

	/**
	 * @param yPos0 the yPos to set
	 */
	private void setyPos0(float yPos0) {
		this.yPos0 = yPos0;
	}
	
	/**
	 * @return the xPos
	 */
	public float getxPos(float t) {
		return (getxDir()*t) + xPos0;
	}

	/**
	 * @return the yPos
	 */
	public float getyPos(float t) {
		return (getyDir()*t) + yPos0;
	}

	/**
	 * @return the xDir
	 */
	public float getxDir() {
		return xDir;
	}

	/**
	 * @return the yDir
	 */
	public float getyDir() {
		return yDir;
	}

	/**
	 * @param angulo the angulo to set
	 */
	private void setAngulo(float angulo) {
		this.xDir = (float) (Math.cos(angulo)/16);
		this.yDir = (float) (Math.sin(angulo)/16);
	}

	public void setxyDir(float xDir, float yDir) {
		this.xDir = xDir;
		this.yDir = yDir;
	}

	/**
	 * @return the manual
	 */
	public boolean isManual() {
		return manual;
	}

	/**
	 * @param manual the manual to set
	 */
	public void setManual(boolean manual) {
		this.manual = manual;
	}	
}
